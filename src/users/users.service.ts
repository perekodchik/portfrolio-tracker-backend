import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from './schemas/user.schema';
import { Model } from 'mongoose';
import { CreateUserDto } from './data-transfer-objects/create-user.dto';
import { UpdateUserDto } from './data-transfer-objects/update-user.dto';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private usersModel: Model<User>) {}

  async create(createUserDto: CreateUserDto): Promise<UserDocument> {
    const createdUser = new this.usersModel(createUserDto);
    return createdUser.save();
  }

  async findById(id: string): Promise<UserDocument | null> {
    return this.usersModel.findById(id).exec();
  }

  async findByUsername(username: string): Promise<UserDocument | null> {
    const user = await this.usersModel.findOne({ username }).exec();
    return user;
  }

  async findAll(): Promise<UserDocument[]> {
    const users = await this.usersModel.find().exec();
    return users;
  }

  async update(
    id: string,
    updateUserDto: UpdateUserDto,
  ): Promise<UserDocument | null> {
    return this.usersModel
      .findByIdAndUpdate(id, updateUserDto, { new: true })
      .exec();
  }

  async remove(id: string): Promise<UserDocument | null> {
    return this.usersModel.findByIdAndDelete(id).exec();
  }
}
