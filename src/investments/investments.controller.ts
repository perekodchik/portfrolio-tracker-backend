import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
} from '@nestjs/common';
import { AddPropertyDto } from './dto/add-property-dto';

@Controller('investments')
export class InvestmentsController {
  @Post('/properties')
  @HttpCode(HttpStatus.CREATED)
  createUser(@Body() addPropertyDto: AddPropertyDto) {
    return addPropertyDto;
  }

  @Get()
  async findAll() {
    return [];
  }
}
