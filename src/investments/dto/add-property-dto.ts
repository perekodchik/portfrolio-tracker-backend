import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsDate,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { Date } from 'mongoose';

export enum RentTypes {
  monthly = 'monthly',
  daily = 'daily',
  custom = 'custom',
}

export class RentInfoDto {
  @IsNumber()
  defaultRentPrice: number;

  @IsEnum(RentTypes)
  rentType: 'daily' | 'monthly' | 'custom';
}

export class AddPropertyDto {
  @IsString()
  name: string;

  @IsNumber()
  price: number;

  @IsDate()
  purchased_at: Date;

  @IsString()
  @IsOptional()
  address: string;

  @IsString()
  @IsOptional()
  picture: string;

  @IsBoolean()
  isForRent: boolean;

  @Type(() => RentInfoDto)
  @IsOptional()
  rentInfo: RentInfoDto;
}
