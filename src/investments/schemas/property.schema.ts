import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Date } from 'mongoose';

const defaultImage =
  'https://images.unsplash.com/photo-1560518883-ce09059eeffa?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=3773&q=80';

// const PropertySchema = {
// 	name: string,
// 	priceHistory: PriceHistorySchema[],
// 	address?: string,
// 	picture?: string //url
// 	isForRent: boolean,
// 	rentInfo?: RentSchema
// }

// const RentSchema = {
// 	defaultRentPrice: number,
// 	depositAmount: number | null,
// 	rentHistory: RentHistorySchema[],
// 	rentFinances: RentFinancesSchema[]
// }

// const RentHistorySchema = {
// 	type: 'daily' | 'monthy',
// 	startDate?: Date,
// 	endDate?: Date,
// 	days: number,
// 	profit: number,
// 	isFromDeposit: boolean
// }

// const RentFinancesSchema = {
// 	type: 'profit' | 'expense' | 'deposit-keep',
// 	amount: number,
// 	description?: string,
// }

@Schema()
export class RentHistory {
  @Prop({ type: String, enum: ['daily', 'monthy'] })
  type: 'daily' | 'monthly';

  @Prop()
  startDate: Date;

  @Prop()
  endDate: Date;

  @Prop()
  days: number;

  @Prop()
  profit: number;

  @Prop({ required: false })
  isFromDeposit: boolean;
}

export const RentHistorySchema = SchemaFactory.createForClass(RentHistory);

@Schema()
export class Rent {
  @Prop({ default: 0 })
  defaultRentPrice: number;

  @Prop({ default: 0 })
  depositAmount: number;

  @Prop({ type: [RentHistorySchema] })
  rentHistory: RentHistory[];
}

const RentSchema = SchemaFactory.createForClass(Rent);

@Schema()
export class PriceHistory {
  @Prop({ type: Date })
  date: Date;

  @Prop()
  price: number;
}

const PriceHistorySchema = SchemaFactory.createForClass(PriceHistory);

@Schema()
export class Property {
  @Prop()
  name: string;

  @Prop({ type: [PriceHistorySchema], default: [] })
  priceHistory: PriceHistory[];

  @Prop({ required: false })
  address: string;

  @Prop({ default: defaultImage })
  picture: string;

  @Prop({ default: false })
  isForRent: boolean;

  @Prop({ type: RentSchema })
  rentInfo: Rent;
}
