import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AddPropertyDto } from './dto/add-property-dto';
import { User } from 'src/users/schemas/user.schema';

@Injectable()
export class InvestmentsService {
  constructor(@InjectModel(User.name) private usersModel: Model<User>) {}

  async addProperty(addPropertyDto: AddPropertyDto) {
    return addPropertyDto;
  }
}
