export interface JwtRequest extends Request {
  user: {
    userId: string;
    username: string;
  };
}

export interface RefreshRequest extends Request {
  user: {
    sub: string;
    username: string;
    iat: number;
    exp: number;
    refreshToken: string;
  };
}
